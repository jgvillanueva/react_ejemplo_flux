import React from 'react';
import * as ColorActions from '../actions/colorActions';

class Botonera extends React.Component{
    constructor(props) {
        super(props);
    }

    handleClick(colorValue){
        console.log('Recibido click');
        ColorActions.changeColor(colorValue);
    }

    render() {
        return (
            <div className="botonera" >
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="navbar-brand">Navbar</div>
                    <ul className="navbar-nav ">
                        <li className="nav-item">
                            <div className="nav-link active" aria-current="page"
                               onClick={this.handleClick.bind(this, '#ff0000')}
                            >Rojo</div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link " aria-current="page"
                               onClick={this.handleClick.bind(this, '#00ff00')}
                            >Verde</div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link " aria-current="page"
                               onClick={this.handleClick.bind(this, '#0000ff')}
                            >Azul</div>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Botonera;
