import dispatcher from "../dispatcher";
import { EventEmitter } from 'events';
import * as ColorAppActions from '../actions/colorActions';

class ColorAppStore extends EventEmitter {
    constructor() {
        super();

        // inicializador del estado
        this.activeColor = '#ff0000';
    }

    // setter del estado
    handleActions(action){
        console.log(action);
        switch (action.type) {
            case ColorAppActions.COLOR_APP_ACTIONS.CHANGE_COLOR:
                this.activeColor = action.value;
                this.emit('storeUpdated');
                break;

            default:


        }
    }

    // getter del estado
    getActiveColor() {
        return this.activeColor;
    }
}

const colorAppStore = new ColorAppStore();
dispatcher.register(colorAppStore.handleActions.bind(colorAppStore));
export default colorAppStore;
