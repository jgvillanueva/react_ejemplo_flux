import React from 'react';
import colorAppStore from "../stores/ColorAppStore";

class Caja extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            color: colorAppStore.getActiveColor()
        }

        this.updateBackgroundColor = this.updateBackgroundColor.bind(this);
    }

    componentDidMount() {
        colorAppStore.on('storeUpdated', this.updateBackgroundColor);
    }

    componentWillUnmount() {
        colorAppStore.removeListener('storeUpdated', this.updateBackgroundColor);
    }

    updateBackgroundColor() {
        this.setState({
            color: colorAppStore.getActiveColor()
        })
    }


    render() {
        return (
            <div className="row justify-content-md-center m-3">
                <div className="card col-4" style={{
                    backgroundColor: this.state.color
                }}>
                    <h1>Caja</h1>
                </div>
            </div>

        )
    }
}

export default Caja;
