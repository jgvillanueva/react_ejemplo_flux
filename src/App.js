
import './App.css';
import Botonera from "./Botonera/Botonera";
import Caja from "./Caja/Caja";
import 'bootstrap/dist/css/bootstrap.css';

import React from 'react';

function App() {
  return (
    <div className="App">
        <h1>React-redux</h1>
      <Botonera />
      <Caja />
    </div>
  );
}

export default App;
